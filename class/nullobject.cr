module NullObjectTest
  class Abc
    property a : String
    property b : Int32

    def initialize(@a = "hi", @b = 15)
    end
  end

  class NullAbc
    property a : Nil
    property b : Nil

    def initialize
      @a = nil
      @b = nil
    end
  end

  def self.get_abc(key : String) : Abc | NullAbc
    return Abc.new("xyz", rand(1..15)) if key.includes? 'a'
    NullAbc.new
  end

  def self.execute_test
    a1 = get_abc "xyzabc"
    a1.a.inspect
    a1.b.inspect

    a2 = get_abc "defghi"
    a2.a.inspect
    a2.b.inspect
  end
end
