module NilTest
  class Abc
    property a : String
    property b : Int32

    def initialize(@a = "hi", @b = 15)
    end
  end

  def self.get_abc(key : String) : Abc?
    return Abc.new("xyz", rand(1..15)) if key.includes? 'a'
    nil
  end

  def self.execute_test
    a1 = get_abc "xyzabc"
    a1.try(&.a).inspect
    a1.try(&.b).inspect

    a2 = get_abc "defghi"
    a2.try(&.a).inspect
    a2.try(&.b).inspect
  end
end
