require "benchmark"
require "./nil"
require "./nullobject"

Benchmark.ips do |bm|
  bm.report "nil" {NilTest.execute_test}
  bm.report "null object" {NullObjectTest.execute_test}
end
