puts "Building nullobject.cr..."
`crystal build --release nullobject.cr`
puts "Building nil.cr..."
`crystal build --release nil.cr`

null_object_test_size = File.size(File.join(__DIR__, "nullobject"))
nil_test_size         = File.size(File.join(__DIR__, "nil"))

puts "Null Object test output file size: #{null_object_test_size} bytes."
puts "Nil test output file size: #{nil_test_size} bytes."

ary = [null_object_test_size, nil_test_size]

puts "Size difference: #{ary.max - ary.min} bytes."
