# Nil vs Null Object pattern Crystal benchmark

There are two directories: `class` and `struct`, one contains a test using Classes and another one using Structs.

Run `ips_benchmark.cr` to run an Instructions Per Second benchmark on both tests.  
Run `size_benchmark.cr` to run an output binary size benchmark on both tests.
